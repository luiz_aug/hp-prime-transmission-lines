# HP Prime - Transmission Lines
In this project the goal is to develop programs to support Transmission Lines problem solving using the HP Prime calculator.

## BER_BEI
This program is used to evaluate the electrical parameters of a transmission line due to the skin effect. Note that a bessel function is included to support numerical evaluation.

## Transitorio
This program is used to print a number of reflections over time in a transmission line. This uses default App Statistics to provide a graphical user interface (GUI). A message box alerts the user about the conventions used to display the results. In this preliminar version they are only available in portuguese.

## Software installation
As the source code is provided, the best method I have found to install they is to connect the calculation on the computer, copying and pasting it usign the programm editor provided by the HP Prime connectivity kit app.

## Further information about HP PPL 
Most of the information needed to code this was taken from [this manual](http://www.hp-prime.de/files/composite_file/file/201-programming-in-hp-ppl.pdf).
A rather fast language overview can also be found [here](https://en.hpprime.club/articles/hans-hp-prime-programming-introduction/).